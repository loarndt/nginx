#!/bin/bash

mkdir -p /var/www/images

docker run -v /var/www:/usr/share/nginx/html:ro -v /var/nginx/conf/:/etc/nginx:ro -p80:80  -d nginx
